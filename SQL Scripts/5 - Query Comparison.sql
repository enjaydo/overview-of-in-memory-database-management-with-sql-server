/*********************************************************************
Step 2 -- Overview
-- Page Compressed BTree table v/s Columnstore table performance differences
-- Enable actual Query Plan in order to see Plan differences when Executing
*/
-- Ensure Database is in 130 compatibility mode
SET STATISTICS IO, TIME ON
GO

-- Execute analytic type query against the transaction tables for baseline
SELECT
  DATEPART(YEAR, H.OrderDate) AS Year
  ,PC.Name AS EnglishProductCategoryName
  ,FirstName + ' ' + LastName AS FullName
  ,count((H.SalesOrderID % 10000000)) AS NumSales
  ,sum(H.Subtotal) AS TotalSalesAmt
  ,Avg(H.Subtotal) AS AvgSalesAmt
  ,count(DISTINCT (H.SalesOrderID % 10000000)) AS NumOrders
  ,count(DISTINCT C.CustomerID) AS CountCustomers
FROM SalesLT.SalesOrderHeaderXL AS H
JOIN SalesLT.SalesOrderDetailXL AS D
  ON D.SalesOrderID = H.SalesOrderID
JOIN SalesLT.Product AS P
  ON P.ProductID = D.ProductID
JOIN SalesLT.Customer AS C
  ON C.CustomerID = H.CustomerID
JOIN SalesLT.ProductCategory AS PSC
  ON PSC.ProductCategoryID = P.ProductCategoryID
JOIN SalesLT.ProductCategory AS PC
  ON PC.ProductCategoryID = PSC.ParentProductCategoryID
WHERE H.OrderDate BETWEEN '1/1/2014' AND '1/1/2015'
  AND PC.Name = 'Components'
GROUP BY PC.Name, DATEPART(YEAR, H.OrderDate), C.CustomerID, C.FirstName, C.LastName
ORDER BY
  DATEPART(YEAR, H.OrderDate),
  FirstName + ' ' + LastName
GO

-- Execute a typical query that joins the Fact Table with dimension tables
-- Note this query will run on the Page Compressed table, Note down the time
SELECT 
  c.Year
  ,F.EnglishProductCategoryName
  ,FirstName + ' ' + LastName AS FullName
  ,count(SalesOrderNumber) AS NumSales
  ,sum(SalesAmount) AS TotalSalesAmt
  ,Avg(SalesAmount) AS AvgSalesAmt
  ,count(DISTINCT SalesOrderNumber) AS NumOrders
  ,count(DISTINCT a.CustomerKey) AS CountCustomers
FROM FactResellerSalesXL_PageCompressed AS a
INNER JOIN DimProduct b ON b.ProductKey = a.ProductKey
INNER JOIN DimCustomer d ON d.CustomerKey = a.CustomerKey
Inner JOIN DimProductSubCategory e on e.ProductSubcategoryKey = b.ProductSubcategoryKey
INNER JOIN DimProductCategory f on f.ProductCategoryKey = E.ProductCategoryKey
INNER JOIN DimDate c ON c.DateKey = a.OrderDateKey
WHERE F.EnglishProductCategoryName = 'Components'
  AND c.FullDateAlternateKey BETWEEN '1/1/2014' AND '1/1/2015'
GROUP BY f.EnglishProductCategoryName,c.Year,d.CustomerKey,d.FirstName,d.LastName
ORDER BY
  c.Year,
  FirstName + ' ' + LastName
GO

-- This is the same Prior query on a table with a Clustered Columnstore index CCI 
-- The comparison numbers are even more dramatic the larger the table is, this is a 11 million row table only.
SELECT 
  c.Year
  ,F.EnglishProductCategoryName
  ,FirstName + ' ' + LastName AS FullName
  ,count(SalesOrderNumber) AS NumSales
  ,sum(SalesAmount) AS TotalSalesAmt
  ,Avg(SalesAmount) AS AvgSalesAmt
  ,count(DISTINCT SalesOrderNumber) AS NumOrders
  ,count(DISTINCT a.CustomerKey) AS CountCustomers
FROM FactResellerSalesXL_CCI AS a
INNER JOIN DimProduct b ON b.ProductKey = a.ProductKey
INNER JOIN DimCustomer d ON d.CustomerKey = a.CustomerKey
Inner JOIN DimProductSubCategory e on e.ProductSubcategoryKey = b.ProductSubcategoryKey
INNER JOIN DimProductCategory f on f.ProductCategoryKey = E.ProductCategoryKey
INNER JOIN DimDate c ON c.DateKey = a.OrderDateKey
WHERE F.EnglishProductCategoryName = 'Components'
  AND c.FullDateAlternateKey BETWEEN '1/1/2014' AND '1/1/2015'
GROUP BY F.EnglishProductCategoryName,c.Year,d.CustomerKey,d.FirstName,d.LastName
ORDER BY
  c.Year,
  FirstName + ' ' + LastName
GO

