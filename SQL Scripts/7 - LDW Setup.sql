IF NOT EXISTS
(
  SELECT
    *
  FROM sys.schemas AS S
  WHERE S.name = 'LDW'
)
BEGIN
  EXEC sys.sp_executesql N'CREATE SCHEMA LDW';
END;

IF OBJECT_ID('LDW.FactSales', 'V') IS NOT NULL
BEGIN
  DROP VIEW LDW.FactSales
END;
GO

CREATE VIEW LDW.FactSales
AS
  SELECT
    H.SalesOrderID,
	DT.OrderDateKey,
	DT.DueDateKey,
    DT.ShipDateKey,
    D.ProductID,
	H.CustomerID,
    --D.OrderQty * (D.UnitPrice - D.UnitPriceDiscount) AS SalesAmount
	H.Subtotal AS SalesAmount
  FROM SalesLT.SalesOrderHeaderXL AS H
  JOIN SalesLT.SalesOrderDetailXL AS D
    ON D.SalesOrderID = H.SalesOrderID
  CROSS APPLY
  (
    SELECT
      CONVERT(VARCHAR(10), H.ShipDate, 112) AS ShipDateKey,
	  CONVERT(VARCHAR(10), H.OrderDate, 112) AS OrderDateKey,
	  CONVERT(VARCHAR(10), H.DueDate, 112) AS DueDateKey
  ) AS DT;
GO

IF OBJECT_ID('LDW.DimDate', 'U') IS NOT NULL
BEGIN
  DROP TABLE LDW.DimDate;
END;

SELECT *
INTO LDW.DimDate
FROM dbo.DimDate

IF OBJECT_ID('LDW.DimProduct', 'V') IS NOT NULL
BEGIN
  DROP VIEW LDW.DimProduct
END;
GO

CREATE VIEW LDW.DimProduct
AS
  SELECT
    P.ProductID,
    P.Name AS ProductName,
    PSC.Name AS ProductSubCategoryName,
    PC.Name AS ProductCategoryName
  FROM SalesLT.Product AS P
  JOIN SalesLT.ProductCategory AS PSC
    ON PSC.ProductCategoryID = P.ProductCategoryID
  --TODO: Should this be LOJ?
  JOIN SalesLT.ProductCategory AS PC
    ON PC.ProductCategoryID = PSC.ParentProductCategoryID;
GO

IF OBJECT_ID('LDW.DimCustomer', 'V') IS NOT NULL
BEGIN
	DROP VIEW LDW.DimCustomer;
END;
GO

CREATE VIEW LDW.DimCustomer
AS
	SELECT 
		C.CustomerID,
		C.FirstName,
		C.LastName
	FROM SalesLT.Customer AS C
GO


SET STATISTICS IO, TIME ON;

SELECT 
  c.Year
  ,B.ProductCategoryName
  ,FirstName + ' ' + LastName AS FullName
  ,count((a.SalesOrderID % 10000000)) AS NumSales
  ,sum(SalesAmount) AS TotalSalesAmt
  ,Avg(SalesAmount) AS AvgSalesAmt
  ,count(DISTINCT (a.SalesOrderID % 10000000)) AS NumOrders
  ,count(DISTINCT a.CustomerID) AS CountCustomers
FROM LDW.FactSales a
INNER JOIN LDW.DimProduct b ON b.ProductID = a.ProductID
INNER JOIN LDW.DimCustomer d ON d.CustomerID = a.CustomerID
--These dimensions were flattened into DimProduct
--Inner JOIN DimProductSubCategory e on e.ProductSubcategoryKey = b.ProductSubcategoryKey
--INNER JOIN DimProductCategory f on f.ProductCategoryID = E.ProductCategoryID
INNER JOIN LDW.DimDate c ON c.DateKey = a.OrderDateKey
WHERE B.ProductCategoryName = 'Components'
    AND c.FullDateAlternateKey BETWEEN '1/1/2014' AND '1/1/2015'
GROUP BY B.ProductCategoryName,c.Year,d.CustomerID,d.FirstName,d.LastName
ORDER BY
	c.Year,
	FirstName + ' ' + LastName
