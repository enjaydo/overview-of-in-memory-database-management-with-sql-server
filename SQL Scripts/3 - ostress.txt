:: Run in RML Command prompt.

:: IMPORTANT: In between executions, run "EXECUTE Demo.usp_DemoReset;"

:: ondisk
ostress.exe -n100 -r5 -Senjaydo-demo.database.windows.net -Unick -PMYrx3wHGRFta@8jiH -dInMemoryDemo -q -Q"DECLARE @i int = 0, @od SalesLT.SalesOrderDetailType_ondisk, @SalesOrderID int, @DueDate datetime2 = sysdatetime(), @CustomerID int = rand() * 8000, @BillToAddressID int = rand() * 10000, @ShipToAddressID int = rand()* 10000; INSERT INTO @od SELECT OrderQty, ProductID FROM Demo.DemoSalesOrderDetailSeed WITH(SNAPSHOT) WHERE OrderID= cast((rand()*60) as int); WHILE (@i < 20) begin; EXECUTE SalesLT.usp_InsertSalesOrder_ondisk @SalesOrderID OUTPUT, @DueDate, @CustomerID, @BillToAddressID, @ShipToAddressID, @od; set @i += 1; end"

:: inmem
ostress.exe -n100 -r5 -Senjaydo-demo.database.windows.net -Unick -PMYrx3wHGRFta@8jiH -dInMemoryDemo -q -Q"DECLARE @i int = 0, @od SalesLT.SalesOrderDetailType_inmem, @SalesOrderID int, @DueDate datetime2 = sysdatetime(), @CustomerID int = rand() * 8000, @BillToAddressID int = rand() * 10000, @ShipToAddressID int = rand()* 10000; INSERT INTO @od SELECT OrderQty, ProductID FROM Demo.DemoSalesOrderDetailSeed WITH(SNAPSHOT) WHERE OrderID= cast((rand()*60) as int); WHILE (@i < 20) begin; EXECUTE SalesLT.usp_InsertSalesOrder_inmem @SalesOrderID OUTPUT, @DueDate, @CustomerID, @BillToAddressID, @ShipToAddressID, @od; set @i += 1; end"