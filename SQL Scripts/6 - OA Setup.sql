--DROP INDEX NCCI_SalesOrderHeader ON SalesLT.SalesOrderHeaderXL
--DROP INDEX NCCI_SalesOrderDetail ON SalesLT.SalesOrderDetailXL

--Covering NCCI for MS Samples
CREATE NONCLUSTERED COLUMNSTORE INDEX NCCI_SalesOrderHeader
ON SalesLT.SalesOrderHeaderXL
(
  SalesOrderID,
  OrderDate,
  Subtotal,
  CustomerID
);

CREATE NONCLUSTERED COLUMNSTORE INDEX NCCI_SalesOrderDetail
ON SalesLT.SalesOrderDetailXL
(
  SalesOrderID,
  ProductID
);
