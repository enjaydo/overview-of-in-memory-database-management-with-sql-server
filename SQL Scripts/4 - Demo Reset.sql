SELECT COUNT(*) AS Header_ondisk FROM SalesLT.SalesOrderHeader_ondisk;
SELECT COUNT(*) AS Detail_ondisk FROM SalesLT.SalesOrderDetail_ondisk;

SELECT COUNT(*) AS Header_inmem FROM SalesLT.SalesOrderHeader_inmem;
SELECT COUNT(*) AS Detail_inmem FROM SalesLT.SalesOrderDetail_inmem;

EXEC Demo.usp_DemoReset;